package resolution.example6.zzeulki.mid_project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class like_list extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_list);


        ListView listView = (ListView) findViewById(R.id.like_list);

        ArrayList<nextvideo> arr = new ArrayList<nextvideo>();

        nextvideo v4 = new nextvideo();
        v4.set_video("보일듯 말듯한 미소! 모앱의 신!","조회수 : 3만",R.drawable.home_1);
        arr.add(v4);
        nextvideo v1 = new nextvideo();
        v1.set_video("Fragment 늪에 빠진 쯜기","조회수: 2436",R.drawable.upload_1);
        arr.add(v1);
        nextvideo v2 = new nextvideo();
        v2.set_video("Youtube 선택을 후회 중?!","조회수: 8473",R.drawable.upload_2);
        arr.add(v2);
        nextvideo v3 = new nextvideo();
        v3.set_video("클래스만 16개!!!! 아직 안끝남!!!","조회수: 2764",R.drawable.home_2);
        arr.add(v3);
        nextvideo v5 = new nextvideo();
        v5.set_video("진짜 구독 좀 해주세요! 이거는 \n진짜 유투브 채널임!","조회수: 4만",R.drawable.sub_1);
        arr.add(v5);


        listAdapter_nextvideo adapter = new listAdapter_nextvideo (this,R.layout.next_video, arr);


        listView.setAdapter(adapter);


    }
}
