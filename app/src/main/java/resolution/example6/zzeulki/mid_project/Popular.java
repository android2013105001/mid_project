package resolution.example6.zzeulki.mid_project;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Popular.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Popular#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Popular extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Popular() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_popular, container, false);
        ImageView iv1 = (ImageView) v.findViewById(R.id.icon1);
        ImageView iv2 = (ImageView) v.findViewById(R.id.icon2);
        ListView listView = (ListView) v.findViewById(R.id.listView);

        ArrayList<data> arr = new ArrayList<data>();

        data d = new data();
        d.data_set("Y.C Park과 함께하는 DB 마스터 되기","Select DDorai * 조회수: 200 ",R.drawable.pop_1,R.drawable.user_icon_2);
        arr.add(d);
        data d2 = new data();
        d2.data_set("너무 쉽죠~ 어떻게 생각해요?","Junhanee ZZang * 조회수: 173 ",R.drawable.pop_2,R.drawable.user_icon_4);
        arr.add(d2);

        listAdapter adapter = new listAdapter (this.getContext(), R.layout.item, arr);

        listView.setAdapter(adapter);



       iv1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(view.getId()==R.id.icon1)
                {
                    Intent i = new Intent(getContext(),category_list.class);
                    startActivity(i);
                }
            }
        });



        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}


