package resolution.example6.zzeulki.mid_project;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link page_video.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link page_video#newInstance} factory method to
 * create an instance of this fragment.
 */
public class page_video extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public page_video() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment page_video.
     */
    // TODO: Rename and change types and number of parameters

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_page_video, container, false);
        ListView listView = (ListView) v.findViewById(R.id.listView_video);

        ArrayList<nextvideo> arr = new ArrayList<nextvideo>();

        nextvideo v1 = new nextvideo();
        v1.set_video("Fragment 늪에 빠진 쯜기","조회수: 2436",R.drawable.upload_1);
        arr.add(v1);
        nextvideo v2 = new nextvideo();
        v2.set_video("Youtube 선택을 후회 중?!","조회수: 8473",R.drawable.upload_2);
        arr.add(v2);
        nextvideo v3 =new nextvideo();
        v3.set_video("클래스만 16개!!!! 아직 안끝남!!!","조회수 : 2764",R.drawable.home_2);
        arr.add(v3);


        listAdapter_nextvideo adapter = new listAdapter_nextvideo (this.getContext(),R.layout.next_video, arr);


        listView.setAdapter(adapter);
        /*listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                {
                    Intent i = new Intent(getContext(),Video.class);
                    startActivity(i);
                }

            }
        });*/
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
