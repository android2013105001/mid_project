package resolution.example6.zzeulki.mid_project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class Video extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        ListView listview1 = (ListView) findViewById(R.id.listView_next_video);
        ListView listview2 = (ListView) findViewById(R.id.listView_reply);

        ArrayList<nextvideo> arr = new ArrayList<nextvideo>();
        ArrayList<reply> arr2 = new ArrayList<reply>();

        nextvideo v1 = new nextvideo();
        v1.set_video("갓명준의 한가한 오후, 여유로운 산책","God MyeongJun  *  조회수 2만",R.drawable.next_vide02);
        arr.add(v1);
        nextvideo v2 = new nextvideo();
        v2.set_video("슈퍼맨에서 발견한 갓의 흔적!","Electronic_Brain  *  조회수 1473",R.drawable.next_video1);
        arr.add(v2);

        reply r1 = new reply();
        r1.reply_set("교수님 강의 너무 재미있어요!!","ZzeulKi   *    3일 전",R.drawable.user_icon_3,R.drawable.good,R.drawable.bad,R.drawable.make);
        arr2.add(r1);
        reply r2 = new reply();
        r2.reply_set("전자두뇌도 울고갑니다 흑흐규ㅠ","Electronic_Brain   *    2시간 전",R.drawable.user_icon_5,R.drawable.good,R.drawable.bad,R.drawable.make);
        arr2.add(r2);
        reply r3 = new reply();
        r3.reply_set("강의 듣고 모앱 마스터 됬어요!","정대2   *   1시간 전",R.drawable.user_icon_7,R.drawable.good,R.drawable.bad,R.drawable.make);
        arr2.add(r3);
        reply r4 = new reply();
        r4.reply_set("안녕하십니까.. 모앱2 강의가 너무 인상적이었습니다.. ","Mr.sake   *   32분 전",R.drawable.user_icon_6,R.drawable.good,R.drawable.bad,R.drawable.make);
        arr2.add(r4);

        listAdapter_nextvideo adapter = new listAdapter_nextvideo (this,R.layout.next_video, arr);
        listAdapter_reply adapter2 = new listAdapter_reply(this,R.layout.reply,arr2);

        listview2.setAdapter(adapter2);
        listview1.setAdapter(adapter);
        listview1.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listview1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                {
                    Intent i = new Intent(getApplicationContext(),Video.class);
                    startActivity(i);
                }

            }
        });

    }
}
