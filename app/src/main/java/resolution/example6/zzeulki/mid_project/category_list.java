package resolution.example6.zzeulki.mid_project;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class category_list extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView text = (TextView) findViewById(R.id.category_name);
        ListView listView = (ListView) findViewById(R.id.listView_category);
        text.setText("데이타베이스");

        ArrayList<data> arr = new ArrayList<data>();

        data d3 = new data();
        d3.data_set("Y.C Park과 함께하는 DB 마스터 되기","Select DDorai * 조회수: 200 ",R.drawable.pop_1,R.drawable.user_icon_2);
        arr.add(d3);
        data d = new data();
        d.data_set("보일듯 말듯한 미소! 모앱의 신!","정순기 교수님과 함께하는 \n즐거운 모바일 앱 프로그래밍",R.drawable.home_1,R.drawable.user_icon_1);
        arr.add(d);
        data d2 = new data();
        d2.data_set("프로젝트를 위한 3일간의 밤샘","모바일 앱 프로그래밍 중간 프로젝트를 위한 \n유투브 분석하기",R.drawable.home_2,R.drawable.user_icon_3);
        arr.add(d2);


        listAdapter adapter = new listAdapter (this ,R.layout.item2, arr);

        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0)
                {
                    Intent i = new Intent(getApplicationContext(),Video.class);
                    startActivity(i);
                }

            }
        });
    }

}
